/**
 * TakeCommand.java
 */
package adventure;

/**
 * Jeder Befehl implementiert die gemeinsame Schnittstelle Execute und ruft
 * seine korrespondierende statische Methode in der Klasse Game auf, welche
 * wiederum die jeweilige Aktion anst�sst.
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class TakeCommand implements IExecute {
	// take()
	private String zweitesWort;

	/**
	 * 
	 */
	public TakeCommand() {
		// TODO noch zu erledigen...
	}

	@Override
	public String execute() {
		Game.take(zweitesWort);
		return zweitesWort;
	}

	@Override
	public void setZweitesWort(String zweitesWort) {
		this.zweitesWort = zweitesWort;
	}
}
