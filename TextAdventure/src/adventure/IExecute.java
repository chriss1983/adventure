/**
 * Execute.java
 */
package adventure;

/**
 * @author CRichter, RSzabo, CWitt
 *
 */
public interface IExecute {
	/**
	 * 
	 * @return execute Command
	 */
	public String execute();

	/**
	 * 
	 * @param zweitesWort
	 */
	public void setZweitesWort(String zweitesWort);
}
