package adventure;

public class Stuff extends Gegenstand {

	public Stuff(String kurzBeschreibung, int gewicht) {
		super(kurzBeschreibung, gewicht);
	}

	@Override
	public boolean isEatable() {
		return false;
	}

	@Override
	public boolean isTakeable() {
		return true;
	}

	@Override
	public boolean hasSecret() {
		return false;
	}

}
