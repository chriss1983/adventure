package adventure;

public class Deko extends Gegenstand {

	public Deko(String kurzBeschreibung) {
		super(kurzBeschreibung, 0);
	}

	@Override
	public boolean isEatable() {
		return false;
	}

	@Override
	public boolean isTakeable() {
		return false;
	}

	@Override
	public boolean hasSecret() {
		return false;
	}

}
