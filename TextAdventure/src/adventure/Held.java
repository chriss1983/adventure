package adventure;

import java.util.LinkedList;

public class Held {
	LinkedList<Gegenstand> inventar;
	private int lebensPunkte;
	private String name;
	private int maxTragbar;

	public Held(String name) {
		this.lebensPunkte = 100;
		this.name = name;
		this.maxTragbar = 20;
		this.inventar = new LinkedList<Gegenstand>();
	}

	/**
	 * 
	 * @return Liefert eine Liste der Gegenst�nde zur�ck @
	 */
	public LinkedList<Gegenstand> getInventar() {
		return inventar;
	}

	public int getLebensPunkte() {
		return this.lebensPunkte;
	}

	public void setLebensPunkte(int lebensPunkte) {
		this.lebensPunkte = lebensPunkte;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxTragbar() {
		return this.maxTragbar;
	}

	public void setMaxTragbar(int maxTragbar) {
		this.maxTragbar = maxTragbar;
	}

	public void setInventar(LinkedList<Gegenstand> inventar) {
		this.inventar = inventar;
	}

}
