package adventure;

public class Secret extends Gegenstand implements ISecretable {

	boolean isAntwort;
	String frage;
	String antwort;
	Gegenstand item;
	public Secret(String kurzBeschreibung,String frage, String antwort, Gegenstand item) {
		super(kurzBeschreibung, 0);
		this.isAntwort = false;
		this.frage = frage;
		this.antwort = antwort;
		this.item = item;
	}

	@Override
	public boolean isEatable() {
		return false;
	}

	@Override
	public boolean isTakeable() {
		return false;
	}

	@Override
	public boolean hasSecret() {
		return true;
	}

	@Override
	public String getRaetzel() {
		return frage;
	}

	@Override
	public Gegenstand getGegestand() {
		return item;
	}

	@Override
	public boolean AntwortIstRichtig(String antwort) {
		if(antwort.equalsIgnoreCase(this.antwort)){
			return true;
		}
		return false;
	}

	@Override
	public void setGegenstand(Gegenstand item) {
		this.item = item;
		
	}

}
