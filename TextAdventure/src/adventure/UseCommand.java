/**
 * UseCommand.java
 */
package adventure;

/**
 * @author CRichter, RSzabo, CWitt
 *
 */
public class UseCommand implements IExecute {
	// use()
	private String zweitesWort;

	/**
	 * 
	 */
	public UseCommand() {
		// TODO noch zu erledigen...
	}

	@Override
	/**
	 * 
	 */
	public String execute() {
		Game.use(zweitesWort);
		return zweitesWort;
	}

	@Override
	/**
	 * 
	 */
	public void setZweitesWort(String zweitesWort) {
		this.zweitesWort = zweitesWort;
	}
}
