package adventure;

public class Key extends Gegenstand {

	Raum raum;
	
	public Key(String kurzBeschreibung, Raum raum) {
		super(kurzBeschreibung, 0);
		this.raum = raum;
	}

	@Override
	public boolean isEatable() {
		return false;
	}

	@Override
	public boolean isTakeable() {
		return true;
	}

	@Override
	public boolean hasSecret() {
		return false;
	}
	public Raum getRaum(){
		return raum;
	}

}
