/**
 * Game.java
 */
package adventure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import startup.Menue;

/**
 * Beinhaltet das Men�, und dient dem Gesamtablauf des Spiels sowie der
 * Erzeugung der Spielwelt
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class Game {
	private static Held held;
	private static Parser parser;
	private static Raum aktuellerRaum;
	private ArrayList<Raum> map;

	/**
	 * 
	 */
	public Game() {
		map = new ArrayList<Raum>();
		parser = new Parser();
		createWorld();
		init();
		gameMenue();
	}

	/**
	 * methode um in den n�chsten raum zu gehen
	 * 
	 * @param richtung
	 *            ist die Richtung die der Raum verlassen werden soll <br />
	 * <br />
	 *            die methode pr�ft ob in der angegebenen richtung ein ausgang
	 *            existiert,<br />
	 *            wenn ja wechelt er den aktuellen raum zu dem hinter dem
	 *            ausgang und gibt<br />
	 *            die beschreibung des raums aus.
	 */
	public static void goRaum(String richtung) {
		if (aktuellerRaum.getAusgaenge().containsKey(richtung)) {
			if (!aktuellerRaum.getAusgaenge().get(richtung).isVerschlossen()) {
				aktuellerRaum = aktuellerRaum.getAusgaenge().get(richtung);
				System.out.println(aktuellerRaum.getKurzBeschreibung());
			} else {
				System.out.println("Abgeschlossen...");
			}

		} else {
			System.out
					.println("An dieser Raumseite befindet sich keine T�r! Suche weiter...");
		}
	}

	/**
	 * methode um die raum beschreibung ausgeben zu lassen printed die aktuelle
	 * raumbeschreibung in die konsole
	 */
	public static void raumBeschreibung() {
		System.out.println(aktuellerRaum.getKurzBeschreibung());
		System.out.println(aktuellerRaum.getLangBeschreibung());
		System.out.println("Vorhandene Sache im Raum:");
		Iterator<String> it = aktuellerRaum.getItemList().values().iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	/**
	 * 
	 */

	public static void take(String gegenstandsname) {
		Iterator<Gegenstand> it = aktuellerRaum.getItemList().keySet()
				.iterator();
		Gegenstand toTake = null;
		while (it.hasNext()) {
			Gegenstand tmp = it.next();
			if (tmp.getKurzBeschreibung().equalsIgnoreCase(gegenstandsname)) {
				if (tmp.isTakeable()) {
					toTake = tmp;
				}
			}
		}
		if (toTake == null) {
			System.out
					.println("Es gibt hier nicht das was Du nehmen m�chtest...");
			System.out
			.println("...oder du darfst es nicht mitnehmen.");
		} else {
			held.getInventar().add(toTake);
			aktuellerRaum.getItemList().remove(toTake);

			System.out.println(toTake.getKurzBeschreibung() + " aufgenommen");
		}

	}

	/**
	 * 
	 */
	public static void use(String gegenstandsname) {
		Iterator<Gegenstand> itr = aktuellerRaum.getItemList().keySet()
				.iterator();
		Iterator<Gegenstand> ith = held.getInventar().iterator();
		Gegenstand toUse = null;
		while (itr.hasNext()) {
			Gegenstand tmp = itr.next();
			if (tmp.getKurzBeschreibung().equalsIgnoreCase(gegenstandsname)) {
				toUse = tmp;
			}
		}
		while (ith.hasNext()) {
			Gegenstand tmp = ith.next();
			if (tmp.getKurzBeschreibung().equalsIgnoreCase(gegenstandsname)) {
				toUse = tmp;
			}
		}
		if (toUse instanceof Food) {
			held.setLebensPunkte(held.getLebensPunkte()
					+ ((Food) toUse).getLive());
		} else if (toUse instanceof Secret) {
			System.out.println(((Secret) toUse).getRaetzel());
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String input = "";
			System.out.print("Anwort: ");
			try {
				input = br.readLine();

			} catch (IOException e) {
				System.out.println("Fehler beim Erfassem der Eingabe: "
						+ e.getMessage());
			}
			if (((Secret) toUse).AntwortIstRichtig(input)) {
				held.getInventar().add(((Secret) toUse).getGegestand());
				System.out.println(((Secret) toUse).getGegestand()
						.getKurzBeschreibung() + " bekommen");
				((Secret) toUse).setGegenstand(null);
			} else {
				System.out.println("Probiere es nochmal...");
			}

		} else if (toUse instanceof Key) {
			if (aktuellerRaum.getAusgaenge().values()
					.contains(((Key) toUse).getRaum())) {
				((Key) toUse).getRaum().setVerschlossen(false);
				held.getInventar().remove(toUse);
				System.out.println("Es hat sich eine T�r ge�ffnet... ");
			} else {
				System.out.println("Es passierte nichts...");
			}
		} else if (toUse instanceof Stuff) {
			System.out.println("Mit " + toUse.getKurzBeschreibung()+" l�sst sich nicht viel anfangen");
			System.out.println("Mitnehmen geht aber...");
		}
		else if(toUse instanceof Deko){
			System.out.println("Lass dir " + toUse.getKurzBeschreibung() + " doch beschreiben");
		}

		else if (toUse == null) {
			System.out.println("Es gibt nicht das was Du nutzen m�chtest...");
		}

	}

	/**
	 * methode um sich die beschreibung des gegenstands anzeigen zu lassen
	 * 
	 * @param gegenstandsname
	 */
	public static void describe(String gegenstandsname) {
		if (gegenstandsname.equalsIgnoreCase("room")) {
			raumBeschreibung();
		}
		ArrayList<Gegenstand> tmp = new ArrayList<Gegenstand>();
		tmp.addAll(held.getInventar());
		tmp.addAll(aktuellerRaum.getItemList().keySet());
		Iterator<Gegenstand> ittmp = tmp.iterator();
		while (ittmp.hasNext()) {
			Gegenstand gtmp = ittmp.next();
			if (gtmp.getKurzBeschreibung().equalsIgnoreCase(gegenstandsname)) {
				System.out.println(gtmp.getLangBeschreibung());
			}
		}

	}

	public static void help(String egal) {
		Iterator<String> it = parser.getBefehle().keySet().iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	public static void show(String something) {
		if (something.equalsIgnoreCase("inventar")
				|| something.equalsIgnoreCase("inventory")) {
			Iterator<Gegenstand> it = held.inventar.iterator();
			while (it.hasNext()) {
				Gegenstand tmp = it.next();
				System.out.println(tmp.getKurzBeschreibung());
			}
		}
	}

	public static void drop(String gegenstandsname) {
		Iterator<Gegenstand> it = held.getInventar().iterator();
		Gegenstand toDrop = null;
		while (it.hasNext()) {
			Gegenstand tmp = it.next();
			if (tmp.getKurzBeschreibung().equalsIgnoreCase(gegenstandsname)) {
				toDrop = tmp;
			}
		}
		if (toDrop == null) {
			System.out
					.println("Du kannst nicht fallenlassen was du nicht hast...");
		} else {
			aktuellerRaum.getItemList().put(toDrop,
					toDrop.getKurzBeschreibung() + " von dir hier abgelegt");
			held.getInventar().remove(toDrop);

			System.out
					.println(toDrop.getKurzBeschreibung() + " fallengelassen");
		}
	}

	/**
	 * 
	 */
	private void createWorld() {
		File raeume = new File("docs/raum.txt");
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(raeume));
			String line;
			while ((line = br.readLine()) != null) {
				String[] split = line.split(";");
				map.add(new Raum(split[0], split[1], Boolean
						.parseBoolean(split[2])));
			}

			map.get(0).addAusgang(map.get(1), "east");
			map.get(0).addAusgang(map.get(2), "south");
			map.get(1).addAusgang(map.get(0), "west");
			map.get(2).addAusgang(map.get(0), "north");
			map.get(2).addAusgang(map.get(3), "south");
			map.get(3).addAusgang(map.get(2), "north");
			map.get(3).addAusgang(map.get(4), "east");
			map.get(4).addAusgang(map.get(3), "west");
			map.get(4).addAusgang(map.get(5), "south");

		} catch (IOException e) {
			System.out.println("Fehler: " + e.getMessage());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("Fehler beim Laden: " + e.getMessage());
			}

		}
	}

	public ArrayList<Raum> getMap() {
		return map;
	}

	public Parser getParser() {
		return parser;
	}

	public Held getHeld() {
		return held;
	}

	/**
	 * 
	 */
	private void init() {
		parser.setBefehl("go", new GoCommand());
		parser.setBefehl("use", new UseCommand());
		parser.setBefehl("take", new TakeCommand());
		parser.setBefehl("descripe", new DescriptionCommand());
		parser.setBefehl("show", new ShowCommand());
		parser.setBefehl("help", new HelpCommand());
		parser.setBefehl("drop", new DropCommand());
		map.get(0)
				.getItemList()
				.put(new Food("Apfel", 1, 20),
						"Hier liegt ein Apfel der sieht lecker aus...");
		map.get(1)
				.getItemList()
				.put(new Secret("Truhe", "Wie heisst der Held?", "Klaus",
						new Key("Blechschl�ssel", map.get(2))),
						"Eine verschlossene Truhe aber vielleicht doch zu �ffnen...");
		map.get(2)
				.getItemList()
				.put(new Food("Gammelfleisch", 2,-100),"Da liegt Gammelfleisch... ob man das essen kann?");
		
		map.get(3)
				.getItemList()
				.put(new Secret("Schublade", "1+1 = ?", "2", new Key(
						"Haust�rschl�ssel", map.get(5))),
						"Eine Schublade ist noch in der K�chenzeile...");
		map.get(3)
				.getItemList()
				.put(new Stuff("Zettel", 1),
						"Ein alter Zettel liegt auf dem Boden");
		map.get(4)
				.getItemList()
				.put(new Deko("Bild"),
						"An der Wand h�ngt ein Bild eines Hauses mit sch�nem Garten");

		held = new Held("Klaus");
		aktuellerRaum = map.get(0);
	}

	/**
	 * 
	 */
	private void gameMenue() {

		Menue.printMenue();
		System.out
				.println("Die Geschichte beginnt in einem dunklen kalten Raum und Klaus ist ganz allein...");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = "";
		while (!input.equalsIgnoreCase("exit")
				&& !aktuellerRaum.getKurzBeschreibung().equals("Drau�en") && !(held.getLebensPunkte() <= 0) ) {

			System.out.print(held.getName() + ": ");
			try {
				input = br.readLine();

			} catch (IOException e) {
				System.out.println("Fehler beim Erfassem der Eingabe: "
						+ e.getMessage());
			}
			if (input.equalsIgnoreCase("exit")) {

			} else if (input.isEmpty()) {
				System.out.println("Hmmm?");
			} else if (input.split(" ").length != 2) {
				if (input.equalsIgnoreCase("help")) {
					input = "help me";
					parser.getCommand(input).execute();
				}
			} else if (parser.getBefehle().containsKey(input.split(" ")[0])) {
				parser.getCommand(input).execute();
			} else {
				System.out.println("Ich habe ihre Eingabe nicht verstanden...");
			}

		}
		if (aktuellerRaum.getKurzBeschreibung().equals("Drau�en")) {
			System.out.println("Geschafft! Herzlichen Gl�ckwunsch!!!");
		}else if(held.getLebensPunkte() <= 0){
			System.out.println("Du bist leider gestorben...");
		}
		System.out.println("Bis zum n�chsten mal bei Fearless Klaus");
	}

}
