/**
 * DropCommand.java
 */
package adventure;

/**
 * Jeder Befehl implementiert die gemeinsame Schnittstelle Execute und ruft
 * seine korrespondierende statische Methode in der Klasse Game auf, welche
 * wiederum die jeweilige Aktion anst�sst.
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class DropCommand implements IExecute
{
	private String zweitesWort;
	
	/**
	 * 
	 */
	public DropCommand() 
	{
		
	}

	@Override
	/**
	 * 
	 */
	public String execute() 
	{
		Game.drop(zweitesWort);
		return zweitesWort;
	}

	@Override
	/**
	 * 
	 */
	public void setZweitesWort(String zweitesWort) 
	{
		this.zweitesWort = zweitesWort;
	}
}
