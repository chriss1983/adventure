/**
 * Gegenstand.java
 */
package adventure;

/**
 * Ein Gegenstand kann sich in einem bestimmten Raum befinden und ist
 * einzigartig. Er kann entweder vom Helden mitgenommen werden(dann verst�rkt er
 * seine F�higkeiten oder Lebensenergie etc.) oder direkt f�r eine Aktion
 * benutzt werden (manchmal muss daf�r ein R�tsel gel�st werden) oder dient als
 * Hinweis/Information
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public abstract class Gegenstand {
	private String kurzBeschreibung;
	private String langBeschreibung;
	// weitere Attribute von Gegenstaenden
	private int gewicht;

	// eigentlich w�re f�r weitere Attribute ein extra Datentyp evtl eine gute
	// Idee
	// darin k�nnten die jeweiligen zus�tzlichen werte gehalten werden
	// bei Gegenstaenden, die keine Verbesserungen am Helden verursachen w�ren
	// diese
	// Attribute null etc.... alle infos zu Gegenst�nden sollten aus
	// einer/mehreren
	// Dateien kommen, zB eine Datei f�r benutzbare Gegenstaende, eine f�r
	// Gegenstaende fuer
	// Aktionen etc

	/**
	 * Ein einzigartiger Gegenstand....
	 */
	public Gegenstand(String kurzBeschreibung, int gewicht) {
		this.kurzBeschreibung = kurzBeschreibung;
	
	}

	/**
	 * 
	 * @return kurze Beschreibung des Gegenstandes
	 */
	public String getKurzBeschreibung() {
		return this.kurzBeschreibung;
	}

	/**
	 * 
	 * @return lange Beschreibung des Gegenstandes
	 */
	public String getLangBeschreibung() {
		return this.langBeschreibung;
	}

	/**
	 * 
	 * @return das Gewicht des Gegenstandes
	 */
	public int getGewicht() {
		return this.gewicht;
	}
	public abstract boolean isEatable();

	public abstract boolean isTakeable();

	public abstract boolean hasSecret();

}
