/**
 * ISecretable.java
 */
package adventure;

/**
 * @author CRichter, RSzabo, CWitt
 *
 */
public interface ISecretable {
	String getRaetzel();
	Gegenstand getGegestand();
	void setGegenstand(Gegenstand item);
	boolean AntwortIstRichtig(String antwort);
	
}
