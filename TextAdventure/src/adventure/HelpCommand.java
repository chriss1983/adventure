/**
 * HelpCommand.java
 */
package adventure;

/**
 * Jeder Befehl implementiert die gemeinsame Schnittstelle Execute und ruft
 * seine korrespondierende statische Methode in der Klasse Game auf, welche
 * wiederum die jeweilige Aktion anst�sst.
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class HelpCommand implements IExecute
{
	private String zweitesWort;
	
	/**
	 * 
	 */
	public HelpCommand() 
	{
		
	}

	@Override
	/**
	 * 
	 */
	public String execute() 
	{
		Game.help(zweitesWort);
		return zweitesWort;
	}

	@Override
	/**
	 * 
	 */
	public void setZweitesWort(String zweitesWort) 
	{
		this.zweitesWort = zweitesWort;
	}
}

