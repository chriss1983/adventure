package adventure;

public class Food extends Gegenstand {

	private int leben;
	public Food(String kurzBeschreibung, int gewicht, int leben) {
		super(kurzBeschreibung, gewicht);
		this.leben = leben;
	}

	@Override
	public boolean isEatable() {
		return true;
	}

	@Override
	public boolean isTakeable() {
		return true;
	}

	@Override
	public boolean hasSecret() {
		return false;
	}

	public int getLive(){
		return leben;
	}
}
