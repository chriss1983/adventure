/**
 * ShowCommand.java
 */
package adventure;

/**
 * Jeder Befehl implementiert die gemeinsame Schnittstelle Execute und ruft
 * seine korrespondierende statische Methode in der Klasse Game auf, welche
 * wiederum die jeweilige Aktion anst�sst.
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class ShowCommand implements IExecute
{
	private String zweitesWort;
	
	/**
	 * 
	 */
	public ShowCommand() 
	{
		
	}

	@Override
	/**
	 * 
	 */
	public String execute() 
	{
		Game.show(zweitesWort);
		return zweitesWort;
	}

	@Override
	/**
	 * 
	 */
	public void setZweitesWort(String zweitesWort) 
	{
		this.zweitesWort = zweitesWort;
	}
}
