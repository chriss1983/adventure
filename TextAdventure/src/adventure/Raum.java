/**
 * Raum.java
 */
package adventure;

import java.util.HashMap;

/**
 * R�ume bestehen aus mind. einem Ausgang in einer Himmelsrichtung. Sie k�nnen
 * Gegenst�nde beinhalten, welche vom Helden je nach Situation
 * eingesammelt/benutzt werden k�nnen oder als blosse Informationsquelle dienen
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class Raum {
	private HashMap<String, Raum> ausgaenge;
	private HashMap<Gegenstand, String> itemList;
	private String kurzBeschreibung;
	private String langBeschreibung;
	private String name;
	private boolean verschlossen;

	/**
	 * 
	 */
	public Raum() {

	}

	/**
	 * 
	 */
	public Raum(String kurzBeschreibung, String langBeschreibung , boolean verschlossen) {
		ausgaenge = new HashMap<String, Raum>();
		itemList = new HashMap<Gegenstand, String>();
		this.kurzBeschreibung = kurzBeschreibung;
		this.langBeschreibung = langBeschreibung;
		this.verschlossen = verschlossen;
	}

	/**
	 * 
	 * @return Liste der Gegenstaende im Raum
	 */
	public HashMap<Gegenstand, String> getItemList() {
		return this.itemList;
	}

	/**
	 * 
	 * @return kurze Beschreibung des Raums
	 */
	public String getKurzBeschreibung() {
		return this.kurzBeschreibung;
	}

	/**
	 * 
	 * @return lange Beschreibung des Raums
	 */
	public String getLangBeschreibung() {
		return this.langBeschreibung;
	}

	/**
	 * 
	 * @return Name des Raums
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 
	 * @param langBeschreibung
	 */
	public void setLangBeschreibung(String langBeschreibung) {
		this.langBeschreibung = langBeschreibung;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param raum
	 * @param richtung
	 * @return null wenn Kombination aus Richtung und Raum noch nicht
	 *         eingetragen war, ansonsten wird der alte Raum zur�ckgegeben, der
	 *         durch den "neuen" ersetzt wurde
	 */
	public Raum addAusgang(Raum raum, String richtung) {
		return ausgaenge.put(richtung, raum);
	}

	/**
	 * 
	 * @return alle Ausgaenge aus dem Raum
	 */
	public HashMap<String, Raum> getAusgaenge() {
		// TODO noch zu erledigen...
		return ausgaenge;
	}

	/**
	 * 
	 * @param richtung
	 * @return Ausgang in einer bestimmten Richtung null, wenn Ausgang nicht
	 *         existiert
	 */
	public Raum getAusgang(String richtung) {
		return ausgaenge.get(richtung);
	}

	public boolean isVerschlossen() {
		return verschlossen;
	}

	public void setVerschlossen(boolean verschlossen) {
		this.verschlossen = verschlossen;
	}

}
