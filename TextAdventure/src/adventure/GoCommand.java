/**
 * GoCommand.java
 */
package adventure;

/**
 * Jeder Befehl implementiert die gemeinsame Schnittstelle Execute und ruft
 * seine korrespondierende statische Methode in der Klasse Game auf, welche
 * wiederum die jeweilige Aktion anst�sst.
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class GoCommand implements IExecute {
	private String zweiterBefehl;

	/**
	 * 
	 */
	public GoCommand() {
		// this.zweiterBefehl = zweiterBefehl;
	}

	@Override
	/**
	 * 
	 */
	public String execute() {
		Game.goRaum(zweiterBefehl);
		return zweiterBefehl;

	}

	@Override
	/**
	 * 
	 */
	public void setZweitesWort(String zweitesWort) {
		this.zweiterBefehl = zweitesWort;
	}
}
