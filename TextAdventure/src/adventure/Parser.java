/**
 * Parser.java
 */
package adventure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Interpretiert einen eingegebenen Befehl und gibt das passende Command-Objekt
 * zur�ck
 * 
 * @author CRichter, RSzabo, CWitt
 *
 */
public class Parser {
	// HashMap welche den Befehlt und die entsprechende Klasse enth�llt
	private HashMap<String, IExecute> befehle = new HashMap<String, IExecute>();

	// Kontruktor
	public Parser() {

	}

	/**
	 * 
	 * @param command
	 * @return
	 */
	public IExecute getCommand(String command) {

		// String welcher die eingegebenen Befehle enth�llt
		ArrayList<String> tmp = new ArrayList<String>();

		// parsen der Usereingabe in "Wort1" u "Wort2"
		StringTokenizer str = new StringTokenizer(command);

		while (str.hasMoreElements()) {

			tmp.add((String) str.nextElement());
		}
		befehle.get(tmp.get(0)).setZweitesWort(tmp.get(1));
		return befehle.get(tmp.get(0));
	}

	/**
	 * 
	 * @param command
	 * @param order
	 */
	public void setBefehl(String command, IExecute order) {
		befehle.put(command, order);
	}

	public HashMap<String, IExecute> getBefehle()
	{
		return this.befehle;
	}
	
	

}
